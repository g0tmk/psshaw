Sketchup::require('sketchup')

#load 'parabolic.rb'

if not file_loaded?(__FILE__)
	UI.menu("Plugins").add_item("generate paraboloid section") {def_par}

	
	toolbar = UI::Toolbar.new "Paraboloid Tools"
	
	asp = UI::Command.new("Add paraboloid section") {def_par}
	asp.small_icon = "sphere.png"
	asp.large_icon = "sphere.png"
	asp.tooltip = "Add paraboloid section"
	asp.status_bar_text = "Generating..."
	asp.menu_text = ""
	
	toolbar = toolbar.add_item asp
	sleep 0.01
	toolbar.show
end
file_loaded(__FILE__) 

def draw_rays (face,center, theta, phi)
	normal=face.normal
	r=1
	
	sunvec=Geom::Vector3d.new(
		r*Math.cos(phi)*Math.sin(theta),
		r*Math.sin(phi)*Math.sin(theta),
		r*Math.cos(theta))
	#print sunvec.to_s	
    
	reflectvec = reflect(sunvec, normal)
	reflectvec.length=5
	sunvec.length= 5
	normal.length=0.25
	#sunvec_point=Geom::Point3d.new(sunvec)[0],sunvec[1],sunvec[2])
	$entities.add_line(vectToPoint(center), vectToPoint(center+normal))
	$entities.add_line(vectToPoint(center), vectToPoint(center+sunvec))
	$entities.add_line(vectToPoint(center), vectToPoint(center+reflectvec))
	
end

def reflect ( v , n )
#	return v2 + ( v1 - v2 )
	#R = 2*(V dot N)*N - V
	r = n
	r.length = r.length * 2 * (v.dot n)
	r = r - v
	return r
end

def vectToPoint v
	return Geom::Point3d.new(v[0], v[1], v[2])
end
def def_par
	$group = Sketchup.active_model.active_entities.add_group
	$entities = $group.entities
	prompts = 	[ 'Inner Radius: ','Outer Radius: ', 'theta min ', 'theta max'] + 
				['radial resolution:','circumfrencial resolution:', 'sun theta degrees' ,'sun phi degrees']
	list=['','','','','', '','', '']			
	defaults = [0.5 , 1.0, -45, 45, 10 , 10, 0.0, 45]
	#r1 r2, deltatheta rres, cres=UI.inputbox(prompts, defaults,list, 'I CAN HAS CHEEZBURGER?:')
	results=UI.inputbox(prompts, defaults,list, 'I CAN HAS CHEEZBURGER?:')
	
	r1=results[0]
	r2=results[1]
	thetamin=results[2].degrees
	thetamax=results[3].degrees
	rres=results[4]
	cres=results[5]
	sun_theta=results[7].degrees
   sun_phi=results[6].degrees
	
	cstep=(thetamax-thetamin)/cres
	rstep=(r2-r1)/rres
	theta=thetamin
	
	while theta < thetamax  do
		r=r1
		#print "theta advanced\n"
		while r < r2-rstep do
		#print "r advanced\n"
		face = $entities.add_face(
		[r*Math.cos(theta),r*Math.sin(theta),r**2],
		[(r+rstep)*Math.cos(theta),(r+rstep)*Math.sin(theta),(r+rstep) ** 2],

		[(r+rstep)*Math.cos(theta+cstep),(r+rstep)*Math.sin(theta+cstep),(r+rstep) ** 2],
		[r*Math.cos(theta+cstep),r*Math.sin(theta+cstep),r**2]
		)

		center=Geom::Vector3d.new(
((r+rstep)*Math.cos(theta+cstep)+r*Math.cos(theta))/2,
((r+rstep)*Math.sin(theta+cstep)+r*Math.sin(theta))/2,
			(((r+rstep) ** 2) +((r) ** 2))/2)
		draw_rays(face, center, sun_theta, sun_phi)
		r=r+rstep
		end
		theta= theta + cstep
	end
	#point = Geom::Point3d.new(0, 0, 0)
	#cpoint = $entities.add_cpoint(point)
	print "parabola defined"
end

